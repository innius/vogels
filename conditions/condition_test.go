package conditions

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
)

func TestWithComparison(t *testing.T) {
	res, err := New().Comparison("version", "=", "10").Build()
	exp := "(#v = :v)"
	assert.Nil(t, err)
	assert.Equal(t, exp, res.String())
	assert.Equal(t, map[string]*string{"#v": aws.String("version")}, res.AttributeNames())
	assert.Contains(t, res.AttributeValues(), ":v")
}

func TestWithTwoComparisons(t *testing.T) {
	res, err := New().Comparison("version", "=", "10").Comparison("time","=",7).Build()
	exp := "(#v = :v) and (#t = :t)"
	assert.Nil(t, err)
	assert.Equal(t, exp, res.String())
	assert.Contains(t, res.AttributeValues(), ":v")
	assert.Contains(t, res.AttributeValues(), ":t")
}
