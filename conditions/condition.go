package conditions

import (
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type ConditionExpression struct {
	expression     []string
	values         map[string]*dynamodb.AttributeValue
	attributeNames map[string]*string
}

func (e *ConditionExpression) String() string {
	return strings.Join(e.expression, " and ")
}

func (e *ConditionExpression) AttributeValues() map[string]*dynamodb.AttributeValue {
	return e.values
}

func (e *ConditionExpression) AttributeNames() map[string]*string {
	return e.attributeNames
}

type condition struct {
	comparator string
	path       string
	value      interface{}
}

func (c *condition) ppath() string {
	return "#" + c.path[:1]
}

func (c *condition) pvalue() string {
	return ":" + c.path[:1]
}

func New() *Builder {
	return &Builder{}
}

type Builder struct {
	conditions []*condition
}

func (b *Builder) Comparison(path, comparator string, value interface{}) *Builder {
	c := &condition{path: path, comparator: comparator, value: value}
	b.conditions = append(b.conditions, c)
	return b
}

func (b *Builder) Build() (*ConditionExpression, error) {
	res := &ConditionExpression{
		expression:     []string{},
		values:         make(map[string]*dynamodb.AttributeValue),
		attributeNames: make(map[string]*string),
	}

	for _, c := range b.conditions {
		// "#version = :v", map[string]interface{}{":v": v}}}
		res.expression = append(res.expression, fmt.Sprintf("(%s %s %s)", c.ppath(), c.comparator, c.pvalue()))

		res.attributeNames[c.ppath()] = aws.String(c.path)
		if value, err := dynamodbattribute.ConvertTo(c.value); err != nil {
			return nil, err
		} else {
			res.values[c.pvalue()] = value
		}
	}

	return res, nil
}
