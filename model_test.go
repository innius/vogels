package vogels

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/stretchr/testify/assert"
)

func TestNewUpdateRequestPointer(t *testing.T) {
	ts := struct{ Name string }{"test123"}

	f := func() {
		NewUpdateRequest(&ts)
	}

	assert.Panics(t, f)
}

func TestNewUpdateRequestNoPointer(t *testing.T) {
	ts := struct{ Name string }{"test123"}

	f := func() {
		NewUpdateRequest(ts)
	}

	assert.NotPanics(t, f)
}

func DefineModel(t *testing.T) {
	md := &ModelDefinition{
		TableName: "my_table",
		Key:       NewKey("name", StringType),
	}
	m := Define(nil, md)

	assert.Equal(t, m.Capacity.ReadCapacityUnits, 1)
	assert.Equal(t, m.Capacity.WriteCapacityUnits, 1)
	assert.Equal(t, m.Key.Hash.Name, "name")
	assert.Equal(t, m.Key.Hash.Type, StringType)
	assert.Nil(t, m.Key.Range)
}

func TestKeySchema(t *testing.T) {
	md := &ModelDefinition{
		TableName: "my_table",
		Key:       NewCompositeKey("name", StringType, "email", StringType),
	}
	m := Define(nil, md)

	act := m.Key.keySchema()

	exp := []*dynamodb.KeySchemaElement{
		{AttributeName: aws.String("name"), KeyType: aws.String("HASH")},
		{AttributeName: aws.String("email"), KeyType: aws.String("RANGE")},
	}
	assert.EqualValues(t, exp, act)
}

func TestAttributeDefinitions(t *testing.T) {
	m := Define(nil, &ModelDefinition{
		TableName: "gamescore",
		Key:       NewCompositeKey("userid", StringType, "gametitle", StringType),
		Indexes: []*Index{&Index{
			Name: "GameTitleIndex",
			Key:  NewCompositeKey("gametitle", StringType, "topscore", StringType),
			Type: GlobalIndex,
		}},
	})
	exp := []*dynamodb.AttributeDefinition{
		{AttributeName: aws.String("userid"), AttributeType: aws.String("S")},
		{AttributeName: aws.String("gametitle"), AttributeType: aws.String("S")},
		{AttributeName: aws.String("topscore"), AttributeType: aws.String("S")},
	}
	act := m.attributeDefinitions()
	assert.Len(t, act, len(exp))
	for i := range exp {
		assert.Contains(t, act, exp[i])
	}
}

func TestKeyAttributeValues(t *testing.T) {
	key := NewCompositeKey("name", StringType, "email", StringType)
	assert.EqualValues(t, []*dynamodb.AttributeDefinition{
		{AttributeName: aws.String("name"), AttributeType: aws.String("S")},
		{AttributeName: aws.String("email"), AttributeType: aws.String("S")},
	}, key.attributeDefinitions())
}

//TODO : local secondary indexes are not supported
func TestGlobalSecondaryIndexes(t *testing.T) {
	md := &ModelDefinition{
		TableName: "my_table",
		Key:       NewCompositeKey("name", StringType, "email", StringType),
		Indexes: []*Index{
			{Name: "global_index", Key: NewCompositeKey("name", StringType, "email", StringType), Type: GlobalIndex},
		},
	}
	m := Define(nil, md)

	assert.EqualValues(t, []*dynamodb.GlobalSecondaryIndex{
		{
			IndexName: aws.String("global_index"),
			KeySchema: []*dynamodb.KeySchemaElement{
				{
					AttributeName: aws.String("name"),
					KeyType:       aws.String("HASH"),
				},
				{
					AttributeName: aws.String("email"),
					KeyType:       aws.String("RANGE"),
				},
			},
			Projection: &dynamodb.Projection{
				ProjectionType: aws.String(dynamodb.ProjectionTypeAll),
			},
			ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
				ReadCapacityUnits:  aws.Int64(1),
				WriteCapacityUnits: aws.Int64(1),
			},
		},
	}, m.globalSecondaryIndexes())
}
