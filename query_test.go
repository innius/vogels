package vogels

import (
	"testing"

	"fmt"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"bitbucket.org/innius/go-trn/uuid"
	"github.com/stretchr/testify/assert"
)

func TestBetweenQuery(t *testing.T) {
	md := &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	}
	q := Define(nil, md).Query("hashvalue")
	c := q.Where("attr").Between("beginning", "ending").(*query)

	res, err := c.build()
	assert.Nil(t, err)

	result1 := *res.KeyConditionExpression
	begin := ""
	end := ""
	for key, value := range res.ExpressionAttributeValues {
		switch *value.S {
		case "beginning":
			begin = key
		case "ending":
			end = key
		default:
		}
	}
	assert.NotEqual(t, "", begin)
	assert.NotEqual(t, "", end)
	expected1 := fmt.Sprintf("#h = :hash AND #f0 BETWEEN %s AND %s", begin, end)
	assert.Equal(t, expected1, result1)
}

func TestInQuery(t *testing.T) {
	md := &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	}
	q := Define(nil, md).Query("hashvalue")
	strings := []string{"beginning", "middle", "ending"}
	interfaces := make([]interface{}, 3)
	for i := 0; i < 3; i++ {
		interfaces[i] = strings[i]
	}
	c := q.Filter("attr").In(interfaces).(*query)

	res, err := c.build()
	assert.Nil(t, err)

	result1 := *res.FilterExpression
	begin := ""
	end := ""
	middle := ""
	for key, value := range res.ExpressionAttributeValues {
		switch *value.S {
		case "beginning":
			begin = key
		case "ending":
			end = key
		case "middle":
			middle = key
		default:
		}
	}
	assert.NotEqual(t, "", begin)
	assert.NotEqual(t, "", end)
	assert.NotEqual(t, "", middle)
	expected1 := fmt.Sprintf("#f0 IN (%v,%v,%v)", begin, middle, end)
	assert.Equal(t, expected1, result1)
}

func TestEqualsQuery(t *testing.T) {
	md := &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	}
	q := Define(nil, md).Query("hashvalue")
	c := q.Filter("attr").Equals("something").(*query)

	res, err := c.build()
	assert.Nil(t, err)

	result1 := *res.FilterExpression
	something := ""
	for key, value := range res.ExpressionAttributeValues {
		switch *value.S {
		case "something":
			something = key
		default:
		}
	}
	assert.NotEqual(t, "", something)
	expected1 := fmt.Sprintf("#f0 = %v", something)
	assert.Equal(t, expected1, result1)
}

func TestGTQuery(t *testing.T) {
	md := &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	}
	q := Define(nil, md).Query("hashvalue")
	c := q.Filter("attr").GreaterThan(5).(*query)
	testSingularComparison(t, ">", c)
}

func TestGTOEQuery(t *testing.T) {
	md := &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	}
	q := Define(nil, md).Query("hashvalue")
	c := q.Filter("attr").GreaterThanOrEqualTo(5).(*query)
	testSingularComparison(t, ">=", c)
}

func TestLTQuery(t *testing.T) {
	md := &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	}
	q := Define(nil, md).Query("hashvalue")
	c := q.Filter("attr").LessThan(5).(*query)
	testSingularComparison(t, "<", c)
}

func TestLTOEQuery(t *testing.T) {
	md := &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	}
	q := Define(nil, md).Query("hashvalue")
	c := q.Filter("attr").LessThanOrEqualTo(5).(*query)
	testSingularComparison(t, "<=", c)
}

func testSingularComparison(t *testing.T, comparison string, c *query) {
	res, err := c.build()
	assert.Nil(t, err)
	result1 := *res.FilterExpression

	something := ""
	for key, value := range res.ExpressionAttributeValues {
		if value.N != nil {
			switch *value.N {
			case "5":
				something = key
			default:
			}
		}
	}
	assert.NotEqual(t, "", something)
	expected1 := fmt.Sprintf("#f0 "+comparison+" %v", something)
	assert.Equal(t, expected1, result1)
}

func TestBuildKeyCondition(t *testing.T) {
	m := Define(nil, &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	})

	q := m.Query("hashvalue").(*query)

	attrs := map[string]*dynamodb.AttributeValue{}
	names := map[string]*string{}

	e, err := q.buildKeyConditionExpr(q.Key, attrs, names)

	assert.Nil(t, err)
	assert.NotNil(t, e)
	assert.Equal(t, "#h = :hash", *e)
	assert.Contains(t, names, "#h")
	assert.Equal(t, "hashkey", *names["#h"])
	assert.Contains(t, attrs, ":hash")
}

func TestWithIndex(t *testing.T) {
	m := Define(nil, &ModelDefinition{
		Key: NewKey("hashkey", StringType),
		Indexes: []*Index{
			{Name: "global", Key: NewCompositeKey("id", "S", "name", "S")},
		},
	})

	q := m.Query("hashvalue").UsingIndex("global").(*query)
	bq, err := q.build()
	assert.Nil(t, err)
	assert.Equal(t, "id", *bq.ExpressionAttributeNames["#h"])
}

func TestWithAttributes(t *testing.T) {
	m := Define(nil, &ModelDefinition{
		Key: NewKey("hashkey", StringType),
	})

	names := map[string]*string{}

	q := m.Query("hashvalue").Attributes("session", "name", "id").(*query)

	exp := map[string]string{"#p0": "session", "#p1": "name", "#p2": "id"}

	expr := q.attributes.build(names)
	assert.Len(t, names, len(exp))

	for s := range exp {
		assert.Contains(t, names, s)
		assert.Equal(t, exp[s], *names[s])
	}
	assert.Equal(t, "#p0,#p1,#p2", *expr)
}

func TestUnmarshal(t *testing.T) {
	type person struct {
		Identity int    `json:"identity"`
		Domain   string `json:"domain"`
	}

	count := 3
	items := make([]map[string]*dynamodb.AttributeValue, count)
	for i := 0; i < count; i++ {
		p := person{i, uuid.NewV4().String()}
		a, _ := dynamodbattribute.MarshalMap(p)
		items[i] = a
	}

	o := &dynamodb.QueryOutput{
		Items: items,
	}
	var res []person
	assert.Nil(t, unmarshal(o, &res))
	assert.Len(t, res, count)
}
