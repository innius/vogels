package expressions

import (
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

const (
	ADD    = "ADD"
	REMOVE = "REMOVE"
	SET    = "SET"
	DELETE = "DELETE"
	addFromitem = "ADDFROMITEM"
)

// UpdateExpression represents a dynamodb update expression
type UpdateExpression struct {
	expression     map[string][]string
	values         map[string]*dynamodb.AttributeValue
	attributeNames map[string]*string
}

func (e *UpdateExpression) String() string {
	if e.expression == nil {
		return ""
	}
	var result []string = make([]string, len(e.expression))
	i := 0
	for key, value := range e.expression {
		result[i] = key + " " + strings.Join(value, ",")
		i++
	}
	return strings.Join(result, " ")
}

func (e *UpdateExpression) AttributeValues() map[string]*dynamodb.AttributeValue {
	return e.values
}

func (e *UpdateExpression) AttributeNames() map[string]*string {
	return e.attributeNames
}

type field struct {
	action     string
	path       string
	expression string
	value      interface{}
}

// ppath returns the placeholder path of the expression field
func (c *field) ppath() string {
	return "#" + c.path
}

// ppath returns the placeholder value of the expression field
func (c *field) pvalue() string {
	return ":" + c.path
}

// NewBuilder returns a new builder instance
func NewBuilder(item interface{}) *Builder {
	return &Builder{
		item: item,
	}
}

// Builder provides a builder for dynamodb update expressions
type Builder struct {
	item   interface{}
	fields []*field
}

// Field adds a field of an item to the expression; the path corresponds to the json tag of the field
// for example:
// Field("policies") -> reads the value of Policies field of builder.item struct and encodes the value to
// a dynmodb attribute value
func (b *Builder) Field(path string) *Builder {
	f := &field{action: SET, path: path}
	f.expression = f.ppath() + " = " + f.pvalue()
	b.fields = append(b.fields, f)
	return b
}

// Fields adds multiple fields in one shot
func (b *Builder) Fields(path ...string) *Builder {
	for i := range path {
		b.Field(path[i])
	}
	return b
}

// ListAppend adds current value of path to the list
func (b *Builder) ListAppend(path string) *Builder {
	f := &field{action: SET, path: path}
	f.expression = fmt.Sprintf("%s = list_append(%s,%s)", f.ppath(), f.ppath(), f.pvalue())
	b.fields = append(b.fields, f)
	return b
}

// WithRemove adds an expression for removing values from a map type column;
func (b *Builder) Remove(path string, value ...int) *Builder {
	for i := range value {
		f := &field{action: REMOVE, path: path}
		f.expression = fmt.Sprintf("%s[%v]", f.ppath(), value[i])
		b.fields = append(b.fields, f)
	}
	if len(value) == 0 {
		f := &field{action: REMOVE, path: path}
		f.expression = f.ppath()
		b.fields = append(b.fields, f)
	}
	return b
}

// Add makes a new add expression which makes it possible to increment a field with a specified value
func (b *Builder) Add(path string, value interface{}) *Builder {
	f := &field{action: ADD, path: path, value: value}
	f.expression = fmt.Sprintf("%s %s", f.ppath(), f.pvalue())
	b.fields = append(b.fields, f)
	return b
}

// Makes a new add expression which adds the value of the specified parameter of the item to its currently stored value.
// Useful in cases where the dynamodbav tag is set, such as when an array or map is stored as a set in dynamo.
func (b *Builder) AddFromItem(path string) *Builder {
	f := &field{action: addFromitem, path: path}
	f.expression = fmt.Sprintf("%s %s", f.ppath(), f.pvalue())
	b.fields = append(b.fields, f)
	return b
}


//Makes a delete expression that deletes the current value of the path in the item from the database. Used for deleting values from a set/list or map
func (b *Builder) DeleteFromItem(path string) *Builder  {
	f := &field{action: DELETE, path: path}
	f.expression = fmt.Sprintf("%s %s", f.ppath(), f.pvalue())
	b.fields = append(b.fields, f)
	return b
}

// build the expression
func (b *Builder) Build() (*UpdateExpression, error) {
	values, err := dynamodbattribute.MarshalMap(b.item)
	if err != nil {
		return nil, err
	}

	res := &UpdateExpression{
		expression:     make(map[string][]string),
		values:         make(map[string]*dynamodb.AttributeValue),
		attributeNames: make(map[string]*string),
	}

	for _, f := range b.fields {
		action := convertAction(f.action)
		res.expression[action] = append(res.expression[action], f.expression)
		switch f.action {
		case ADD:
			value, err := dynamodbattribute.Marshal(f.value)
			if err != nil {
			    return nil, err
			}
			res.values[f.pvalue()] = value
		case SET, addFromitem, DELETE:
			res.values[f.pvalue()] = values[f.path]
		}
		res.attributeNames[f.ppath()] = aws.String(f.path)
	}
	return res, nil
}

func convertAction(action string) string {
	if action == addFromitem {
		return ADD
	} else {
		return action
	}
}
