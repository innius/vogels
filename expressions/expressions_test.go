package expressions

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/stretchr/testify/assert"
)

type TestRecord struct {
	Name  string   `json:"Name"`
	Name2 string   `json:"Name2"`
	Tags  []string `json:"Tags"`
}

var testrecord = &TestRecord{
	Name: "Joop", Name2: "van Voorthuizen", Tags: []string{"ceo", "euros", "cash"},
}

func TestSingleSETAction(t *testing.T) {
	res, err := NewBuilder(*testrecord).Field("Name").Build()
	assert.Nil(t, err)
	exp := "SET #Name = :Name"
	assert.Equal(t, exp, res.String())
	assert.Equal(t, res.AttributeNames()["#Name"], aws.String("Name"))
	assert.Equal(t, "{\n  S: \"Joop\"\n}", res.AttributeValues()[":Name"].String())
}

func TestMultipleSETAction(t *testing.T) {
	res, err := NewBuilder(*testrecord).
		Fields("Name", "Name2").
		Build()
	assert.Nil(t, err)
	exp := "SET #Name = :Name,#Name2 = :Name2"
	assert.Equal(t, exp, res.String())
	assert.Equal(t, res.AttributeNames()["#Name"], aws.String("Name"))
	assert.Equal(t, res.AttributeNames()["#Name2"], aws.String("Name2"))

}

func TestListAppend(t *testing.T) {
	res, err := NewBuilder(*testrecord).ListAppend("Tags").Build()
	assert.Nil(t, err)
	exp := "SET #Tags = list_append(#Tags,:Tags)"
	assert.Equal(t, exp, res.String())
	assert.Equal(t, res.AttributeNames()["#Tags"], aws.String("Tags"))
}

func TestRemoveAction(t *testing.T) {
	res, err := NewBuilder(*testrecord).Remove("Name2").Build()
	assert.Nil(t, err)
	exp := "REMOVE #Name2"

	assert.Equal(t, exp, res.String())
	assert.Equal(t, res.AttributeNames()["#Name2"], aws.String("Name2"))
	assert.Empty(t, res.AttributeValues())
}

func TestAddAction(t *testing.T) {
	res, err := NewBuilder(*testrecord).Add("version", 1).Build()
	assert.Nil(t, err)
	exp := "ADD #version :version"

	assert.Equal(t, exp, res.String())
	assert.Equal(t, res.AttributeNames()["#version"], aws.String("version"))
	assert.Equal(t, res.AttributeValues()[":version"].String(), "{\n  N: \"1\"\n}")
}
