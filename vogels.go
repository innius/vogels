package vogels

import (
	"context"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"

	"bitbucket.org/innius/vogels/conditions"
	"bitbucket.org/innius/vogels/expressions"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

func NewUpdateExpressionBuilder(item interface{}) *expressions.Builder {
	return expressions.NewBuilder(item)
}

func NewConditionExpressionBuilder() *conditions.Builder {
	return conditions.New()
}

type ModelAPIV2 interface {
	CreateTable(context.Context) error
	GetItem(c context.Context, v interface{}, hk interface{}, rk interface{}, attrs ...string) (bool, error)
	BatchGet(c context.Context, result interface{}, keys []*ItemKey, attrs ...string) error
	BatchWriteItem(c context.Context, in *batchWriteItemRequest) error
	PutItem(c context.Context, in *putItemRequest) (interface{}, error)
	Update(c context.Context, in *updateItemInput) (interface{}, error)
	Destroy(c context.Context, hash interface{}, rangeKey interface{}) error
	Query(hash interface{}) Query
	Scan(c context.Context, v interface{}) error
	FullScan(c context.Context, v interface{}) error
	FullScanWithFilter(c context.Context, v interface{}, filter expression.Expression) error
}

func NewModelV2(client dynamodbiface.DynamoDBAPI, md *ModelDefinition) ModelAPIV2 {
	return &modelV2{Model: Define(client, md)}
}
