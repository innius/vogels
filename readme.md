# vogels

vogels is a [DynamoDB][2] data mapper for [go][1].

## Features
* Simplified data modeling and mapping to DynamoDB types
* Advanced chainable apis for [query](#query) 
* Data validation
* [Global Secondary Indexes](#global-indexes)

## Installation

```
  go get bitbucket.org/innius/vogels

  include ("bitbucket.org/innius/vogels")
```

### Define a Model

Models are defined through the toplevel define method.
```golang
	type Account struct {
		Email string `json:"email"`
		Name  string `json:"name"`
		Age   int    `json:"age"`
	}

	// define the model 
	model := vogels.NewModelV2(
		&vogels.DefineRequest{
			Name:       "accounts",
			Hashkey:    "email",
			Timestamps: true,
			Type:       reflect.TypeOf(Account{}),
		})
```

Models can also be defined with hash and range keys.

```
	model := vogels.NewModelV2(
		&vogels.DefineRequest{
			Name:     "blogposts",
			Hashkey:  "email",
			RangeKey: "title",
			Type:     reflect.TypeOf(BlogPost{}),
		})
```

### Query

For models that use hash and range keys Vogels provides a flexible and chainable query api

```
// query for blog posts by werner@example.com
BlogPost
  .query('werner@example.com')
  .exec(callback);

// same as above, but load all results
BlogPost
  .query('werner@example.com')
  .loadAll()
  .exec(callback);

// only load the first 5 posts by werner
BlogPost
  .query('werner@example.com')
  .limit(5)
  .exec(callback);

// query for posts by werner where the tile begins with 'Expanding'
BlogPost
  .query('werner@example.com')
  .where('title').beginsWith('Expanding')
  .exec(callback);


```


[1]: http://golang.org
[2]: http://aws.amazon.com/dynamodb
