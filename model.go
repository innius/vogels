package vogels

import (
	"context"
	"fmt"
	"reflect"

	"github.com/aws/aws-sdk-go/service/dynamodb/expression"

	"bitbucket.org/innius/vogels/conditions"
	"bitbucket.org/innius/vogels/expressions"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
)

// Model defines a dynamo db table model
type ModelDefinition struct {
	// The Name of the dynamodb table
	TableName string
	// the read / write capacity of the table
	Capacity *Capacity
	// the global / local indexes of the table
	Indexes []*Index
	// the primary key
	Key *Key
}

type modelV2 struct {
	*Model
}

func (m *modelV2) GetItem(c context.Context, v interface{}, hk interface{}, rk interface{}, attrs ...string) (bool, error) {
	return m.Model.getItemWithContext(c, v, hk, rk, attrs...)
}

func (m *modelV2) PutItem(c context.Context, in *putItemRequest) (interface{}, error) {
	return m.Model.putItemWithContext(c, in)
}

func (m *modelV2) Update(c context.Context, in *updateItemInput) (interface{}, error) {
	return m.Model.updateWithContext(c, in)
}

type Model struct {
	client dynamodbiface.DynamoDBAPI
	*ModelDefinition
}

// Capacity defines the throughput for a table / index
type Capacity struct {
	ReadCapacityUnits  int64
	WriteCapacityUnits int64
}

// Key defines a dynamo db key
type Key struct {
	Hash  *KeyField
	Range *KeyField
}

// defines a key field with its datatype
type KeyField struct {
	Name string
	Type FieldType
}

// defines the type of a keyfield
type FieldType string

// defines the type of an index
type IndexType string

// KeyItem is a key of a single item
type ItemKey struct {
	Hash  interface{}
	Range interface{}
}

const (
	StringType FieldType = dynamodb.ScalarAttributeTypeS
	NumberType FieldType = dynamodb.ScalarAttributeTypeN

	GlobalIndex IndexType = "global"
	LocalIndex  IndexType = "local"
)

// defines a dynamodb index
type Index struct {
	Name     string
	Key      *Key
	Capacity *Capacity
	Type     IndexType
}

// returns the dynamodb attribute definitions for the model
func (m *Model) attributeDefinitions() []*dynamodb.AttributeDefinition {
	attrs := map[string]*dynamodb.AttributeDefinition{}
	a := m.Key.attributeDefinitions()
	for i := range a {
		attrs[*a[i].AttributeName] = a[i]
	}
	for _, idx := range m.Indexes {
		// check if the key is already included
		a = idx.Key.attributeDefinitions()
		for i := range a {
			attrs[*a[i].AttributeName] = a[i]
		}
	}

	res := make([]*dynamodb.AttributeDefinition, 0, len(attrs))
	for _, value := range attrs {
		res = append(res, value)
	}
	return res
}

// NewKey creates a new hash key for a given name / type
func NewKey(name string, t FieldType) *Key {
	return &Key{Hash: &KeyField{Name: name, Type: t}}
}

// NewKey creates a new composite key with a hash and a range
func NewCompositeKey(h string, ht FieldType, r string, rt FieldType) *Key {
	return &Key{
		Hash:  &KeyField{Name: h, Type: ht},
		Range: &KeyField{Name: r, Type: rt},
	}
}

// returns the dynamodb keyschema for a key
func (k *Key) keySchema() []*dynamodb.KeySchemaElement {
	ks := []*dynamodb.KeySchemaElement{
		{AttributeName: aws.String(k.Hash.Name), KeyType: aws.String("HASH")},
	}

	if k.Range != nil {
		ks = append(ks, &dynamodb.KeySchemaElement{AttributeName: aws.String(k.Range.Name), KeyType: aws.String("RANGE")})
	}
	return ks
}

// returns the dynamodb attribute definitions for a key
func (k *Key) attributeDefinitions() []*dynamodb.AttributeDefinition {
	attrs := []*dynamodb.AttributeDefinition{
		{
			AttributeName: aws.String(k.Hash.Name),
			AttributeType: aws.String((string)(k.Hash.Type)),
		},
	}
	if k.Range != nil {
		attrs = append(attrs, &dynamodb.AttributeDefinition{
			AttributeName: aws.String(k.Range.Name),
			AttributeType: aws.String((string)(k.Range.Type)),
		})
	}
	return attrs
}

// define a new model
// Deprecated; use NewModelV2
func Define(client dynamodbiface.DynamoDBAPI, md *ModelDefinition) *Model {
	if md.Capacity == nil {
		md.Capacity = &Capacity{1, 1}
	}
	m := &Model{
		client:          client,
		ModelDefinition: md,
	}
	return m
}

func (m *Model) CreateTable(c context.Context) error {

	log.Debug("Creating table")

	// create the table
	req := &dynamodb.CreateTableInput{
		TableName:              &m.TableName,
		KeySchema:              m.Key.keySchema(),
		GlobalSecondaryIndexes: m.globalSecondaryIndexes(),
		LocalSecondaryIndexes:  m.localSecondaryIndexes(),
		AttributeDefinitions:   m.attributeDefinitions(),
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  &m.Capacity.ReadCapacityUnits,
			WriteCapacityUnits: &m.Capacity.WriteCapacityUnits,
		},
	}
	if _, err := m.client.CreateTable(req); err != nil {
		log.Debugf("could not create table %s; Error: %+v", m.TableName, err)
		return err
	}
	err := m.client.WaitUntilTableExistsWithContext(c, &dynamodb.DescribeTableInput{TableName: aws.String(m.TableName)})
	return errors.Wrapf(err, "could not create table %s", m.TableName)
}

func (m *Model) globalSecondaryIndexes() []*dynamodb.GlobalSecondaryIndex {
	var result []*dynamodb.GlobalSecondaryIndex
	for _, idx := range m.Indexes {
		if idx.Type == GlobalIndex {
			if idx.Capacity == nil {
				idx.Capacity = &Capacity{1, 1}
			}
			keySchema := idx.Key.keySchema()
			index := &dynamodb.GlobalSecondaryIndex{
				IndexName:  &idx.Name,
				KeySchema:  keySchema,
				Projection: &dynamodb.Projection{ProjectionType: aws.String("ALL")},
				ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
					ReadCapacityUnits:  &idx.Capacity.ReadCapacityUnits,
					WriteCapacityUnits: &idx.Capacity.WriteCapacityUnits,
				},
			}
			result = append(result, index)
		}
	}
	return result
}

func (m *Model) localSecondaryIndexes() []*dynamodb.LocalSecondaryIndex {
	var result []*dynamodb.LocalSecondaryIndex
	for _, idx := range m.Indexes {
		if idx.Type == LocalIndex {
			if idx.Capacity == nil {
				idx.Capacity = &Capacity{1, 1}
			}
			keySchema := idx.Key.keySchema()
			index := &dynamodb.LocalSecondaryIndex{
				IndexName:  &idx.Name,
				KeySchema:  keySchema,
				Projection: &dynamodb.Projection{ProjectionType: aws.String("ALL")},
			}
			result = append(result, index)
		}
	}
	return result
}

// returns the attribute values for a provided hash / range combination
func (k *Key) keyAttributeValues(hash interface{}, rangeKey interface{}) (map[string]*dynamodb.AttributeValue, error) {
	attrs := map[string]*dynamodb.AttributeValue{}

	if v, err := dynamodbattribute.Marshal(hash); err != nil {
		return nil, errors.Wrapf(err, "could not marshal hash key %v to dynamodb attribute value", hash)
	} else {
		attrs[k.Hash.Name] = v
	}

	if k.Range != nil {
		if v, err := dynamodbattribute.Marshal(rangeKey); err != nil {
			return nil, errors.Wrapf(err, "could not marshal range key %v to dynamodb attribute value", rangeKey)
		} else {
			attrs[k.Range.Name] = v
		}
	}
	return attrs, nil
}

func (m *Model) GetItem(v interface{}, hk interface{}, rk interface{}, attrs ...string) (bool, error) {
	return m.getItemWithContext(context.Background(), v, hk, rk, attrs...)
}

func (m *Model) getItemWithContext(c context.Context, v interface{}, hk interface{}, rk interface{}, attrs ...string) (bool, error) {
	key, err := m.Key.keyAttributeValues(hk, rk)
	if err != nil {
		return false, err
	}
	req := &dynamodb.GetItemInput{
		TableName: aws.String(m.TableName),
		Key:       key,
	}

	if len(attrs) > 0 {
		names := map[string]*string{}
		pe := (projectionExpression)(attrs)
		req.ProjectionExpression = pe.build(names)
		req.ExpressionAttributeNames = names
	}

	output, err := m.client.GetItemWithContext(c, req)
	if err != nil {
		return false, errors.Wrap(err, "GetItem error")
	}

	found := len(output.Item) > 0

	if err := dynamodbattribute.UnmarshalMap(output.Item, v); err != nil {
		return false, errors.Wrap(err, "could not unmarshal GetItem response")
	}

	return found, nil
}

// DEPRICATED: use GetItem
// Get returns a single record by its hash and (optionally) range keys.
// Additionally the attributes to get can be specified
// EXAMPLES
// Get(item, "foo@bar.com", "") returns a record with a hash only
// Get(item, "foo@bar.com", "123", "name", "version") returns a record with specific attributes
func (m *Model) Get(c context.Context, v interface{}, hk interface{}, rk interface{}, attrs ...string) error {
	_, err := m.GetItem(v, hk, rk, attrs...)
	return err
}

//Batch get up to a 100 items
func (m *Model) BatchGet(c context.Context, result interface{}, keys []*ItemKey, attrs ...string) error {
	if len(keys) > 100 {
		return errors.New("Too many keys provided. The limit is 100")
	}
	dynKeys := []map[string]*dynamodb.AttributeValue{}
	for _, key := range keys {
		dynKey, err := m.Key.keyAttributeValues(key.Hash, key.Range)
		if err != nil {
			return err
		}
		dynKeys = append(dynKeys, dynKey)
	}

	req := func() *dynamodb.BatchGetItemInput {
		if len(attrs) > 0 {
			names := map[string]*string{}
			pe := (projectionExpression)(attrs)
			pex := pe.build(names)
			return &dynamodb.BatchGetItemInput{
				RequestItems: map[string]*dynamodb.KeysAndAttributes{
					m.TableName: {
						Keys:                     dynKeys,
						ProjectionExpression:     pex,
						ExpressionAttributeNames: names,
					},
				},
			}
		} else {
			return &dynamodb.BatchGetItemInput{
				RequestItems: map[string]*dynamodb.KeysAndAttributes{
					m.TableName: {
						Keys: dynKeys,
					},
				},
			}
		}
	}()

	if output, err := m.client.BatchGetItemWithContext(c, req); err != nil {
		return errors.Wrap(err, "BatchGetItem error")
	} else {
		resValue := resultValue(result)
		slice := makeSlice(resValue)
		//TODO: Handle UnprocessedKeys
		for _, document := range output.Responses[m.TableName] {
			t := newValue(resValue)
			err := dynamodbattribute.UnmarshalMap(document, t.Addr().Interface())
			if err != nil {
				return errors.Wrap(err, "could not unmarshal dynamodb Scan result")
			}
			slice = reflect.Append(slice, t)
		}
		resValue.Set(slice)
		if err != nil {
			return errors.Wrap(err, "could not unmarshal GetItem response")
		}
	}
	return nil
}

func NewPutItemRequest(v interface{}) *putItemRequest {
	return &putItemRequest{
		item: checkValueType(v),
	}
}

type putItemRequest struct {
	item interface{}
}

func (m *Model) PutItem(in *putItemRequest) (interface{}, error) {
	return m.putItemWithContext(context.Background(), in)
}

func (m *Model) putItemWithContext(c context.Context, in *putItemRequest) (interface{}, error) {
	attrmap, err := dynamodbattribute.MarshalMap(in.item)
	if err != nil {
		return nil, err
	}

	input := &dynamodb.PutItemInput{
		TableName: aws.String(m.TableName),
		Item:      attrmap,
	}

	log.Debugf("PutItem request %s", input.String())

	output, err := m.client.PutItemWithContext(c, input)
	if err != nil {
		return nil, err
	}
	log.Debugf("PutItem response %s", output.String())

	return unmarshalMap(output.Attributes, in.item)
}

func NewBatchWriteRequest(unprocessedItemsTarget interface{}, items []interface{}) (*batchWriteItemRequest, error) {
	if err := validateBatchWriteItemRequest(unprocessedItemsTarget, items); err != nil {
		return nil, err
	}

	itemSlice := make([]interface{}, len(items))
	for i := range items {
		itemSlice[i] = items[i]
	}

	return &batchWriteItemRequest{
		unprocessedItems: unprocessedItemsTarget,
		items:            itemSlice,
	}, nil
}

func validateBatchWriteItemRequest(unprocessedItemsTarget interface{}, items []interface{}) error {
	up := reflect.ValueOf(unprocessedItemsTarget)

	if up.Kind() != reflect.Ptr {
		return fmt.Errorf("unprocessedItems property must be a pointer")
	}
	if up.IsNil() {
		return fmt.Errorf("unprocessedItems property must not be nil")
	}
	if reflect.Indirect(up).Type().Kind() != reflect.Slice {
		return fmt.Errorf("unprocessedItems property must be a slice of values")
	}

	itemValues := reflect.ValueOf(items)
	if itemValues.IsNil() {
		return fmt.Errorf("items property must not be nil")
	}

	return nil
}

type batchWriteItemRequest struct {
	items            []interface{} // TODO accept interface and convert to slice
	unprocessedItems interface{}
}

func (m *Model) BatchWriteItem(c context.Context, request *batchWriteItemRequest) error {
	return m.batchWriteItem(c, request)
}

func (m *Model) batchWriteResponse(output *dynamodb.BatchWriteItemOutput, unprocessedTarget *reflect.Value) error {
	unprocessedItems := output.UnprocessedItems[m.TableName]

	ut := *unprocessedTarget
	resValue := ut.Elem()
	slice := makeSlice(resValue)

	for _, writeRequest := range unprocessedItems {
		attr := writeRequest.PutRequest.Item

		t := newValue(resValue)
		err := dynamodbattribute.UnmarshalMap(attr, t.Addr().Interface())
		if err != nil {
			return errors.Wrap(err, "could not unmarshal dynamodb Scan result")
		}
		slice = reflect.Append(slice, t)
	}
	resValue.Set(slice)

	return nil
}

func (m *Model) batchWriteItem(c context.Context, request *batchWriteItemRequest) error {
	if request == nil {
		return fmt.Errorf("request cannot be nil")
	}

	writeRequests := make([]*dynamodb.WriteRequest, len(request.items))

	for i := range request.items {
		item, err := dynamodbattribute.MarshalMap(request.items[i])
		if err != nil {
			return err
		}

		writeRequests[i] = &dynamodb.WriteRequest{
			PutRequest: &dynamodb.PutRequest{
				Item: item,
			},
		}
	}

	input := dynamodb.BatchWriteItemInput{
		RequestItems: map[string][]*dynamodb.WriteRequest{
			m.TableName: writeRequests,
		},
	}
	output, err := m.client.BatchWriteItemWithContext(c, &input)
	if err != nil {
		return err
	}

	t := reflect.ValueOf(request.unprocessedItems)
	return m.batchWriteResponse(output, &t)
}

type updateItemInput struct {
	Item                interface{}
	UpdateExpression    *expressions.UpdateExpression
	ConditionExpression *conditions.ConditionExpression
}

func NewUpdateRequest(v interface{}) *updateItemInput {
	return &updateItemInput{
		Item: checkValueType(v),
	}
}

func (u *updateItemInput) WithUpdateExpression(uex *expressions.UpdateExpression) *updateItemInput {
	u.UpdateExpression = uex
	return u
}

func (u *updateItemInput) WithConditionExpression(cex *conditions.ConditionExpression) *updateItemInput {
	u.ConditionExpression = cex
	return u
}

func (m *Model) Update(in *updateItemInput) (interface{}, error) {
	return m.updateWithContext(context.Background(), in)
}

func (m *Model) updateWithContext(c context.Context, in *updateItemInput) (interface{}, error) {
	key, err := m.keyAttributeValuesForItem(in.Item)
	if err != nil {
		return nil, err
	}
	req := &dynamodb.UpdateItemInput{
		TableName:    aws.String(m.TableName),
		Key:          key,
		ReturnValues: aws.String(dynamodb.ReturnValueAllNew),
	}
	if in.UpdateExpression != nil {
		req.UpdateExpression = aws.String(in.UpdateExpression.String())
		if av := in.UpdateExpression.AttributeValues(); len(av) > 0 {
			req.ExpressionAttributeValues = av
		}

		if an := in.UpdateExpression.AttributeNames(); len(an) > 0 {
			req.ExpressionAttributeNames = an
		}
	}

	// specify the condition expressions
	if in.ConditionExpression != nil {
		req.ConditionExpression = aws.String(in.ConditionExpression.String())

		iatt := in.ConditionExpression.AttributeValues()
		if req.ExpressionAttributeValues == nil {
			req.ExpressionAttributeValues = iatt
		}
		for k, _ := range iatt {
			req.ExpressionAttributeValues[k] = iatt[k]
		}

		inam := in.ConditionExpression.AttributeNames()
		if req.ExpressionAttributeNames == nil {
			req.ExpressionAttributeNames = inam
		}
		for k, _ := range inam {
			req.ExpressionAttributeNames[k] = inam[k]
		}
	}
	log.Debugf("UpdateItem request: %s", req.String())
	output, err := m.client.UpdateItemWithContext(c, req)
	if err != nil {
		return nil, err
	}
	log.Debugf("UpdateItem response: %s", output.String())

	return unmarshalMap(output.Attributes, in.Item)
}

func (m *Model) Destroy(c context.Context, hash interface{}, rangeKey interface{}) error {
	key, err := m.Key.keyAttributeValues(hash, rangeKey)
	if err != nil {
		return err
	}
	req := &dynamodb.DeleteItemInput{
		TableName: aws.String(m.TableName),
		Key:       key,
	}
	if _, err := m.client.DeleteItemWithContext(c, req); err != nil {
		log.Error("delete item failed with error %+V", err)
		return err
	}
	return nil
}

// unmarshal dynamodb attribute values to a new instance of type v
func unmarshalMap(attrs map[string]*dynamodb.AttributeValue, v interface{}) (interface{}, error) {
	res := reflect.New(reflect.TypeOf(v)).Interface()

	err := dynamodbattribute.UnmarshalMap(attrs, res)
	if err != nil {
		return nil, err
	}
	return res, nil
}

// returns an map with dynamodb key attribute values for a given item
func (m *Model) keyAttributeValuesForItem(item interface{}) (map[string]*dynamodb.AttributeValue, error) {
	// marshal struct to  a map of attributes
	attrs, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		return nil, errors.Wrap(err, "could not marshal item to map")
	}
	// get the key attributes from the map
	res := map[string]*dynamodb.AttributeValue{}
	res[m.Key.Hash.Name] = attrs[m.Key.Hash.Name]

	if m.Key.Range != nil {
		res[m.Key.Range.Name] = attrs[m.Key.Range.Name]
	}

	return res, nil
}

func (m *Model) Query(hash interface{}) Query {
	return newQuery(m, hash)
}

func checkValueType(v interface{}) interface{} {
	vv := reflect.ValueOf(v)

	if vv.Kind() == reflect.Struct {
		return v
	}
	panic(fmt.Errorf("value must be of type struct; got %s", vv.Kind().String()))
}

func (m *Model) Scan(c context.Context, v interface{}) error {
	req := &dynamodb.ScanInput{
		TableName: aws.String(m.TableName),
	}
	output, err := m.client.ScanWithContext(c, req)
	//TODO this only works up to 1000 entries
	err = unmarshalScan(output, v)
	if err != nil {
		return errors.Wrap(err, "could not unmarshal Scan response")
	}
	return nil
}

//Uses scan pagination to allow result sets > 1MB
func (m *modelV2) FullScan(c context.Context, v interface{}) error {
	req := &dynamodb.ScanInput{
		TableName: aws.String(m.TableName),
	}
	result := resultValue(v)
	slice := makeSlice(result)

	err := m.client.ScanPagesWithContext(c, req,
		func(page *dynamodb.ScanOutput, lastPage bool) bool {
			for _, item := range page.Items {
				t := newValue(result)
				if err := dynamodbattribute.UnmarshalMap(item, t.Addr().Interface()); err != nil {
					log.Errorf("could not unmarshal dynamodb Scan result; %+v", err)
				}
				slice = reflect.Append(slice, t)
			}
			return !lastPage
		})
	result.Set(slice)
	return err
}

// unmarshal the scan output to a new value of type v
//TODO merge with unmarshal Query output
func unmarshalScan(o *dynamodb.ScanOutput, v interface{}) error {
	result := resultValue(v)
	slice := makeSlice(result)
	for _, document := range o.Items {
		t := newValue(result)
		err := dynamodbattribute.UnmarshalMap(document, t.Addr().Interface())
		if err != nil {
			return errors.Wrap(err, "could not unmarshal dynamodb Scan result")
		}
		slice = reflect.Append(slice, t)
	}
	result.Set(slice)
	return nil
}

func (m *modelV2) FullScanWithFilter(c context.Context, v interface{}, expr expression.Expression) error {
	req := &dynamodb.ScanInput{
		TableName:                 aws.String(m.TableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
	}
	result := resultValue(v)
	slice := makeSlice(result)

	err := m.client.ScanPagesWithContext(c, req,
		func(page *dynamodb.ScanOutput, lastPage bool) bool {
			for _, item := range page.Items {
				t := newValue(result)
				if err := dynamodbattribute.UnmarshalMap(item, t.Addr().Interface()); err != nil {
					log.Errorf("could not unmarshal dynamodb Scan result; %+v", err)
				}
				slice = reflect.Append(slice, t)
			}
			return !lastPage
		})
	result.Set(slice)
	return err
}
