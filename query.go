// http://docs.aws.amazon.com/sdk-for-go/api/service/dynamodb/DynamoDB.html#Query-instance_method
package vogels

import (
	"context"
	"fmt"
	"reflect"
	"strings"

	"bitbucket.org/innius/go-trn/uuid"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type Query interface {
	UsingIndex(string) Query
	Limit(int) Query
	Where(string) Condition
	Filter(string) FilterCondition
	Descending() Query
	Attributes(...string) Query
	Execute(interface{}) (*QueryResult, error)
	ExecuteWithContext(context.Context, interface{}) (*QueryResult, error)
	//empty string will start from the beginning
	WithStartKey(lastkey string) Query
}

type Condition interface {
	Equals(interface{}) Query
	Between(interface{}, interface{}) Query
	GreaterThan(interface{}) Query
	GreaterThanOrEqualTo(interface{}) Query
	LessThan(interface{}) Query
	LessThanOrEqualTo(interface{}) Query
}

type FilterCondition interface {
	Equals(interface{}) Query
	Between(interface{}, interface{}) Query
	In([]interface{}) Query
	GreaterThan(interface{}) Query
	GreaterThanOrEqualTo(interface{}) Query
	LessThan(interface{}) Query
	LessThanOrEqualTo(interface{}) Query
}

type startKey struct {
	hash     interface{}
	rangeKey interface{}
}

type indexedStartKey struct {
	primaryKey *startKey
	indexKey   *startKey
}

func newQuery(model *Model, hash interface{}) Query {
	return &query{
		hash:  hash,
		Model: model,
	}
}

type query struct {
	*Model
	hash             interface{}
	limit            *int64
	attributes       projectionExpression
	index            *Index
	descending       bool
	keyConditions    []*condition
	filterConditions []*filterCondition
	startKey         string
}

// selects the index for the query by index name
func (q *query) UsingIndex(name string) Query {
	for i := range q.Indexes {
		idx := q.Indexes[i]
		if idx.Name == name {
			q.index = idx
			break
		}
	}
	return q
}

// limits the query result
func (q *query) Limit(n int) Query {
	q.limit = aws.Int64(int64(n))
	return q
}

// ascending / descending
func (q *query) Descending() Query {
	q.descending = true
	return q
}

func (q *query) Where(n string) Condition {
	c := &condition{
		id:   len(q.keyConditions) + len(q.filterConditions),
		name: n,
		q:    q,
	}
	q.keyConditions = append(q.keyConditions, c)
	return c
}

func (q *query) Filter(n string) FilterCondition {
	fc := &filterCondition{
		condition: &condition{
			id:   len(q.keyConditions) + len(q.filterConditions),
			name: n,
			q:    q,
		},
	}
	q.filterConditions = append(q.filterConditions, fc)
	return fc
}

func (q *query) WithStartKey(startkey string) Query {
	q.startKey = startkey
	return q
}

// defines a key condition in a dynamodb query
type condition struct {
	id    int
	q     *query
	name  string
	build func(map[string]*dynamodb.AttributeValue, map[string]*string) string
}

type filterCondition struct {
	*condition
}

func (c *condition) Between(start, end interface{}) Query { //BETWEEN is including edges: https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_Condition.html
	c.build = func(attrs map[string]*dynamodb.AttributeValue, names map[string]*string) string {
		f := ":_" + (uuid.NewV4().String()[:8])
		t := ":_" + (uuid.NewV4().String()[:8])
		h := fmt.Sprintf("#f%d", c.id)
		attrs[f], _ = dynamodbattribute.Marshal(start)
		attrs[t], _ = dynamodbattribute.Marshal(end)
		names[h] = aws.String(c.name)
		return fmt.Sprintf("%v BETWEEN %s AND %s", h, f, t)
	}
	return c.q
}

func (c *condition) Equals(equal interface{}) Query {
	c.build = func(attrs map[string]*dynamodb.AttributeValue, names map[string]*string) string {
		f := ":_" + (uuid.NewV4().String()[:8])
		h := fmt.Sprintf("#f%d", c.id)
		attrs[f], _ = dynamodbattribute.Marshal(equal)
		names[h] = aws.String(c.name)
		return fmt.Sprintf("%v = %s", h, f)
	}
	return c.q
}

func (c *condition) GreaterThan(arg interface{}) Query {
	c.build = func(attrs map[string]*dynamodb.AttributeValue, names map[string]*string) string {
		f := ":_" + (uuid.NewV4().String()[:8])
		h := fmt.Sprintf("#f%d", c.id)
		attrs[f], _ = dynamodbattribute.Marshal(arg)
		names[h] = aws.String(c.name)
		return fmt.Sprintf("%v > %s", h, f)
	}
	return c.q
}

func (c *condition) GreaterThanOrEqualTo(arg interface{}) Query {
	c.build = func(attrs map[string]*dynamodb.AttributeValue, names map[string]*string) string {
		f := ":_" + (uuid.NewV4().String()[:8])
		h := fmt.Sprintf("#f%d", c.id)
		attrs[f], _ = dynamodbattribute.Marshal(arg)
		names[h] = aws.String(c.name)
		return fmt.Sprintf("%v >= %s", h, f)
	}
	return c.q
}

func (c *condition) LessThan(arg interface{}) Query {
	c.build = func(attrs map[string]*dynamodb.AttributeValue, names map[string]*string) string {
		f := ":_" + (uuid.NewV4().String()[:8])
		h := fmt.Sprintf("#f%d", c.id)
		attrs[f], _ = dynamodbattribute.Marshal(arg)
		names[h] = aws.String(c.name)
		return fmt.Sprintf("%v < %s", h, f)
	}
	return c.q
}

func (c *condition) LessThanOrEqualTo(arg interface{}) Query {
	c.build = func(attrs map[string]*dynamodb.AttributeValue, names map[string]*string) string {
		f := ":_" + (uuid.NewV4().String()[:8])
		h := fmt.Sprintf("#f%d", c.id)
		attrs[f], _ = dynamodbattribute.Marshal(arg)
		names[h] = aws.String(c.name)
		return fmt.Sprintf("%v <= %s", h, f)
	}
	return c.q
}

func (fc *filterCondition) In(set []interface{}) Query {
	fc.build = func(attrs map[string]*dynamodb.AttributeValue, names map[string]*string) string {
		h := fmt.Sprintf("#f%d", fc.id)
		names[h] = aws.String(fc.name)
		cond := fmt.Sprintf("%v IN (", h)
		for i, s := range set {
			if i != 0 {
				cond = cond + ","
			}
			f := ":_" + (uuid.NewV4().String()[:8])
			attrs[f], _ = dynamodbattribute.Marshal(s)
			cond = cond + fmt.Sprintf("%v", f)
		}
		return cond + ")"
	}
	return fc.q
}

// Defines the projection expression
func (q *query) Attributes(attrs ...string) Query {
	q.attributes = (projectionExpression)(attrs)
	return q
}

type QueryResult struct {
	Query
	RawResponse *dynamodb.QueryOutput
	LastKey     string
}

// builds the key condition expression for a specified key
// it adds a condition for the hash of the key and the conditions
// attribute values are appended to the attributes map
// attribute names are appended to the names dictionary
func (q *query) buildKeyConditionExpr(k *Key, attrs map[string]*dynamodb.AttributeValue, names map[string]*string) (*string, error) {
	h := "#h"
	hv := ":hash"
	v, err := dynamodbattribute.Marshal(q.hash)
	if err != nil {
		return nil, errors.Wrapf(err, "could not marshal hash key %v", q.hash)
	}

	attrs[hv] = v
	names[h] = aws.String(k.Hash.Name)

	expr := fmt.Sprintf("%v = %v", h, hv)

	// add the key conditions
	kc := []string{expr}
	for _, c := range q.keyConditions {
		kc = append(kc, c.build(attrs, names))
	}
	return aws.String(strings.Join(kc, " AND ")), nil
}

// builds filter expression
// attribute values are appended to the attributes map
// attribute names are appended to the names dictionary
func (q *query) buildFilterConditionExpr(attrs map[string]*dynamodb.AttributeValue, names map[string]*string) (*string, error) {
	// add the filter conditions
	kc := []string{}
	for _, c := range q.filterConditions {
		result := c.build(attrs, names)
		kc = append(kc, result)
	}
	if len(kc) == 0 {
		return nil, nil
	}
	return aws.String(strings.Join(kc, " AND ")), nil
}

type projectionExpression []string

func (pe projectionExpression) build(names map[string]*string) *string {
	c := len(pe)
	if c == 0 {
		return nil
	}
	l := make([]string, c)
	for i, a := range pe {
		s := fmt.Sprintf("#p%d", i)
		names[s] = aws.String(a)
		l[i] = s
	}
	expr := strings.Join(l, ",")

	return aws.String(expr)

}

// builds a query definition to a dynamodb query input
func (q *query) build() (*dynamodb.QueryInput, error) {
	k := q.Model.Key
	if q.index != nil {
		k = q.index.Key
	}
	attrs := map[string]*dynamodb.AttributeValue{}
	names := map[string]*string{}

	ke, err := q.buildKeyConditionExpr(k, attrs, names)
	if err != nil {
		return nil, err
	}
	fe, err := q.buildFilterConditionExpr(attrs, names)
	if err != nil {
		return nil, err
	}

	pe := q.attributes.build(names)

	in := &dynamodb.QueryInput{
		TableName:                 aws.String(q.Model.TableName),
		KeyConditionExpression:    ke,
		ExpressionAttributeNames:  names,
		ExpressionAttributeValues: attrs,
		ProjectionExpression:      pe,
		Limit:                     q.limit,
		FilterExpression:          fe,
	}
	if q.index != nil {
		in.IndexName = aws.String(q.index.Name)
	}
	if q.descending {
		in.ScanIndexForward = aws.Bool(!q.descending)
	}
	if q.startKey != "" {
		in.ExclusiveStartKey = q.createExclusiveStartkey()
	}
	return in, nil
}

func (q *query) Execute(v interface{}) (*QueryResult, error) {
	return q.execute(context.Background(), v)
}

func (q *query) ExecuteWithContext(c context.Context, v interface{}) (*QueryResult, error) {
	return q.execute(c, v)
}

func (q *query) execute(c context.Context, v interface{}) (*QueryResult, error) {
	in, err := q.build()
	if err != nil {
		return nil, err
	}
	logrus.Debug(in.String())
	if resp, err := q.client.QueryWithContext(c, in); err != nil {
		return nil, errors.Wrap(err, "dynamodb query error")
	} else {
		if err := unmarshal(resp, v); err != nil {
			return nil, err
		}
		lastkey := ""

		if q.hasMore(resp) {
			lastkey, err = q.parseLastEvaluatedKey(resp)
		}

		logrus.Debug(resp.String())
		logrus.Debugf("your lastkey is %v", lastkey)
		return &QueryResult{
			Query:       q,
			RawResponse: resp,
			LastKey:     lastkey,
		}, err
	}
}

func (q *query) hasMore(r *dynamodb.QueryOutput) bool {
	limit := aws.Int64Value(q.limit)
	count := aws.Int64Value(r.Count)
	scannedCount := aws.Int64Value(r.ScannedCount)

	return (count >= limit) || scannedCount >= limit
}

// unmarshal the query output to a new value of type v
func unmarshal(o *dynamodb.QueryOutput, v interface{}) error {
	result := resultValue(v)
	slice := makeSlice(result)
	for _, document := range o.Items {
		t := newValue(result)
		err := dynamodbattribute.UnmarshalMap(document, t.Addr().Interface())
		if err != nil {
			return errors.Wrap(err, "could not unmarshal dynamodb query result")
		}

		slice = reflect.Append(slice, t)
	}
	result.Set(slice)
	return nil
}

func resultValue(v interface{}) reflect.Value {
	return reflect.ValueOf(v).Elem()
}

func makeSlice(v reflect.Value) reflect.Value {
	resultSliceType := reflect.SliceOf(v.Type().Elem())
	return reflect.MakeSlice(resultSliceType, 0, 0)
}

func newValue(v reflect.Value) reflect.Value {
	return reflect.New(v.Type().Elem()).Elem()
}

//AWS stores numbers as *string, so using a string should be fine. The only other possibility is bytes, which we currently do not support.
func (q *query) parseLastEvaluatedKey(resp *dynamodb.QueryOutput) (string, error) {
	hash := aws.String("")
	rangeKey := aws.String("")
	if resp.LastEvaluatedKey == nil {
		return "", nil
	}
	err := dynamodbattribute.Unmarshal(resp.LastEvaluatedKey[q.Model.Key.Hash.Name], hash)
	if err != nil {
		return "", errors.Wrap(err, "Could not unmarshal hashkey of LastEvaluatedKey")
	}
	if q.Model.Key.Range != nil {
		err := dynamodbattribute.Unmarshal(resp.LastEvaluatedKey[q.Model.Key.Range.Name], rangeKey)
		if err != nil {
			return "", errors.Wrap(err, "Could not unmarshal rangekey of LastEvaluatedKey")
		}
	}
	if q.index == nil {
		return concatKeys(aws.StringValue(hash), aws.StringValue(rangeKey)), nil
	} else {
		return q.parseLastEvaluatedIndexKey(resp, aws.StringValue(hash), aws.StringValue(rangeKey))
	}
}

func (q *query) parseLastEvaluatedIndexKey(resp *dynamodb.QueryOutput, modelHash, modelRange string) (string, error) {
	hash := aws.String("")
	rangeKey := aws.String("")
	err := dynamodbattribute.Unmarshal(resp.LastEvaluatedKey[q.index.Key.Hash.Name], hash)
	if err != nil {
		return "", errors.Wrap(err, "Could not unmarshal hashkey of LastEvaluatedIndexKey")
	}
	if q.index.Key.Range != nil {
		err := dynamodbattribute.Unmarshal(resp.LastEvaluatedKey[q.index.Key.Range.Name], rangeKey)
		if err != nil {
			return "", errors.Wrap(err, "Could not unmarshal rangekey of LastEvaluatedIndexKey")
		}
	}
	return concatKeys(modelHash, modelRange, aws.StringValue(hash), aws.StringValue(rangeKey)), nil
}

func (q *query) createExclusiveStartkey() map[string]*dynamodb.AttributeValue {
	split := splitKeys(q.startKey)
	result := map[string]*dynamodb.AttributeValue{}
	fillWithKeyValues(result, q.Model.Key, split[0], split[1])
	if len(split) == 4 && q.index != nil {
		fillWithKeyValues(result, q.index.Key, split[2], split[3])
	}
	return result
}

func concatKeys(keys ...string) string {
	concatKey := ""
	for i, s := range keys {
		concatKey = concatKey + s
		if i != len(keys)-1 {
			concatKey = concatKey + "~"
		}
	}
	return concatKey
}

func splitKeys(concatkey string) []string {
	return strings.Split(concatkey, "~")
}

func fillWithKeyValues(target map[string]*dynamodb.AttributeValue, key *Key, hash string, rangeKey string) {
	target[key.Hash.Name] = makeAttrValue(key.Hash.Type, hash)
	if rangeKey != "" {
		target[key.Range.Name] = makeAttrValue(key.Range.Type, rangeKey)
	}
}

func makeAttrValue(keyType FieldType, keyString string) *dynamodb.AttributeValue {
	switch keyType {
	case StringType:
		return &dynamodb.AttributeValue{S: aws.String(keyString)}
	case NumberType:
		return &dynamodb.AttributeValue{N: aws.String(keyString)}
	default:
		return &dynamodb.AttributeValue{S: aws.String(keyString)}
	}
}
