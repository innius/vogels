module bitbucket.org/innius/vogels

go 1.15

require (
	bitbucket.org/innius/go-trn v1.0.4
	github.com/aws/aws-sdk-go v1.16.0
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.4.0
	github.com/stretchr/testify v1.9.0
	golang.org/x/net v0.0.0-20190327214358-63eda1eb0650 // indirect
)
