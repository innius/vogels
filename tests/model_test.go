package tests

import (
	"bitbucket.org/innius/go-trn/uuid"
	"bitbucket.org/innius/vogels"
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"

	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var ctx context.Context

func testClient() dynamodbiface.DynamoDBAPI {
	sess, _ := session.NewSession()
	endpoint := "http://localhost:8000"
	config := &aws.Config{}
	config.Endpoint = &endpoint

	return dynamodb.New(sess, config.WithRegion("us-east-1"))
}

func TestMain(m *testing.M) {
	ctx = context.Background()
	os.Exit(m.Run())
}

type Credit struct {
}

type Account struct {
	Email string   `json:"email"`
	Name  string   `json:"name"`
	Age   int      `json:"age"`
	Tags  []string `json:"tags"`
}

func (a Account) Model() vogels.ModelAPIV2 {
	model := vogels.NewModelV2(
		testClient(),
		&vogels.ModelDefinition{
			TableName: "accounts",
			Key:       vogels.NewKey("email", vogels.StringType),
		})

	return model
}

func TestGetItem(t *testing.T) {
	model := Account{}.Model()
	model.CreateTable(ctx)
	account := &Account{Email: "foo@example.com", Name: "Foo Bar", Age: 21, Tags: []string{"aap", "noot", "mies"}}
	_, err := model.PutItem(context.Background(), vogels.NewPutItemRequest(*account))

	assert.NoError(t, err)

	v := &Account{}
	found, err := model.GetItem(context.Background(), v, account.Email, nil)
	assert.NoError(t, err)
	assert.True(t, found)
	assert.Equal(t, account, v)

	t.Run("Not found", func(t *testing.T) {
		found, err := model.GetItem(context.Background(), v, uuid.NewV4().String(), nil)
		assert.NoError(t, err)
		assert.False(t, found)
	})
}

func TestModelGetWithHashAndRange(t *testing.T) {
	// arrange
	model := BlogPost{}.Model()
	items := createTestPosts(model)

	// act
	item := &BlogPost{}
	found, err := model.GetItem(ctx, item, items[0].Email, items[0].Title)

	// assert
	assert.Nil(t, err)
	assert.True(t, found)
	assert.NotEmpty(t, item)

}

func TestModelUpdate(t *testing.T) {
	// arrange
	model := Account{}.Model()
	model.CreateTable(ctx)
	account := &Account{Email: "foo@example.com", Name: "Foo Bar", Age: 21, Tags: []string{"aap", "noot", "mies"}}
	_, err := model.PutItem(context.Background(), vogels.NewPutItemRequest(*account))
	assert.Nil(t, err)
	account.Age = 23
	account.Tags = append(account.Tags, "kees")
	account.Name = "piet"

	// act
	account.Name = "bla"
	account.Age = 21
	uex, err := vogels.NewUpdateExpressionBuilder(*account).Fields("name", "age", "tags").Build() // Only update these 2 fields
	assert.Nil(t, err)

	req := vogels.NewUpdateRequest(*account).WithUpdateExpression(uex)
	_, err = model.Update(context.Background(), req)
	assert.Nil(t, err)

	// assert
	assert.Nil(t, err)
	result := &Account{}
	found, err := model.GetItem(ctx, result, account.Email, "")
	assert.Nil(t, err)
	assert.True(t, found)
	assert.Equal(t, 21, result.Age)
	assert.Equal(t, account.Tags, result.Tags)
	assert.Equal(t, "bla", result.Name)
}

func TestModelUpdateFromEmptyCollection(t *testing.T) {
	model := Account{}.Model()
	model.CreateTable(ctx)
	account := &Account{Email: "foo@example.com", Name: "Foo Bar", Age: 21, Tags: []string{}}
	_, err := model.PutItem(ctx, vogels.NewPutItemRequest(*account))
	assert.Nil(t, err)

	// Is the model persisted ok?
	result := &Account{}
	found, err := model.GetItem(ctx, result, account.Email, "")
	assert.Nil(t, err)
	assert.True(t, found)
	var exp []string
	assert.Equal(t, exp, result.Tags)
	assert.Equal(t, "Foo Bar", result.Name)

	// Make some changes
	account.Tags = append(account.Tags, "kees")
	account.Name = "Other name"

	// Update the changes
	uex, err := vogels.NewUpdateExpressionBuilder(*account).Fields("name", "tags").Build()
	assert.Nil(t, err)

	req := vogels.NewUpdateRequest(*account).WithUpdateExpression(uex)
	_, err = model.Update(ctx, req)
	assert.Nil(t, err)

	// assert that the data is valid again.
	resultTwo := &Account{}
	found, err = model.GetItem(ctx, resultTwo, account.Email, "")
	assert.Nil(t, err)
	assert.True(t, found)
	assert.Equal(t, []string{"kees"}, resultTwo.Tags)
	assert.Equal(t, "Other name", resultTwo.Name)
}

func TestModelUpdateAdd(t *testing.T) {
	model := Account{}.Model()
	model.CreateTable(ctx)
	account := &Account{Email: "foots@example.com", Name: "Foo Bar", Age: 21, Tags: []string{"aap"}}
	_, err := model.PutItem(ctx, vogels.NewPutItemRequest(*account))

	// Update
	account.Tags = append(account.Tags, "kees", "gijs", "jos")
	account.Age = account.Age + 1
	uex, err := vogels.NewUpdateExpressionBuilder(*account).Fields("name", "age", "tags").Build() // Only update these 2 fields
	assert.Nil(t, err)

	req := vogels.NewUpdateRequest(*account).WithUpdateExpression(uex)
	_, err = model.Update(ctx, req)
	assert.Nil(t, err)
	result := &Account{}

	found, err := model.GetItem(ctx, result, account.Email, "")

	assert.Nil(t, err)
	assert.True(t, found)
	assert.Equal(t, len(account.Tags), len(result.Tags))
	assert.Equal(t, 22, result.Age)
}

func TestModelUpdateWithCondition(t *testing.T) {
	model := Account{}.Model()
	model.CreateTable(ctx)
	account := &Account{Email: "ronvdw@example.com", Name: "Foo Bar", Age: 21, Tags: []string{"aap"}}
	_, err := model.PutItem(ctx, vogels.NewPutItemRequest(*account))
	assert.Nil(t, err)

	// UpdateWithCondition is gone? Does it look like this?
	uex, err := vogels.NewUpdateExpressionBuilder(*account).Add("age", 2).Build()
	assert.Nil(t, err)

	req := vogels.NewUpdateRequest(*account).WithUpdateExpression(uex)
	_, err = model.Update(ctx, req)
	assert.Nil(t, err)

	result := &Account{}
	found, err := model.GetItem(ctx, result, account.Email, "")
	assert.Nil(t, err)
	assert.True(t, found)
	assert.Equal(t, result.Age, 23)
}

func TestModelDestroy(t *testing.T) {
	// arrange
	model := Account{}.Model()
	model.CreateTable(ctx)
	account := &Account{Email: "foo@example.com", Name: "Foo Bar", Age: 21}
	_, err := model.PutItem(ctx, vogels.NewPutItemRequest(*account))
	assert.Nil(t, err)
	result := &Account{}

	// act
	err = model.Destroy(ctx, account.Email, "")

	// assert
	assert.Nil(t, err)
	found, err := model.GetItem(ctx, result, account.Email, "")
	assert.Nil(t, err)
	assert.False(t, found)
}

func TestModelWithNumericKey(t *testing.T) {
	m := vogels.NewModelV2(
		testClient(),
		&vogels.ModelDefinition{
			TableName: "logentries_" + uuid.NewV4().String(),
			Key:       vogels.NewKey("entry_no", vogels.NumberType),
		})

	type entry struct {
		EntryNo int    `json:"entry_no"`
		Message string `json:"message"`
	}

	e := entry{
		10, "foo",
	}

	assert.Nil(t, m.CreateTable(ctx))

	_, err := m.PutItem(ctx, vogels.NewPutItemRequest(e))
	assert.Nil(t, err)

	result := &entry{}
	found, err := m.GetItem(ctx, result, 10, nil)
	assert.True(t, found)
	assert.Nil(t, err)
	assert.EqualValues(t, &e, result)
}

func TestBatchGet(t *testing.T) {
	model, posts := createManyTestPosts(75)
	keys := []*vogels.ItemKey{}
	for _, p := range posts {
		keys = append(keys, &vogels.ItemKey{
			Hash:  p.Email,
			Range: p.Title,
		})
	}
	result := []BlogPost{}
	err := model.BatchGet(context.Background(), &result, keys)
	assert.NoError(t, err)
	assert.Equal(t, 75, len(result))
}

func TestBatchWrite(t *testing.T) {
	model := BlogPost{}.Model()
	model.CreateTable(ctx)

	posts := []BlogPost{
		{
			Email: "foo@bar.com",
			Title: "some title",
			Name:  "my name",
		},
		{
			Email: "bar@foo.com",
			Title: "another title",
			Tags:  nil,
			Name:  "another name",
		},
	}

	out := []BlogPost{}
	input, err := vogels.NewBatchWriteRequest(&out, []interface{}{posts[0], posts[1]})
	assert.Nil(t, err)

	err = model.BatchWriteItem(ctx, input)
	assert.Nil(t, err)

	t.Run("assert items were stored", func(t *testing.T) {
		keys := make([]*vogels.ItemKey, len(posts))
		for i := range posts {
			keys[i] = &vogels.ItemKey{
				Hash:  posts[i].Email,
				Range: posts[i].Title,
			}
		}

		result := []BlogPost{}
		err := model.BatchGet(context.Background(), &result, keys)
		assert.NoError(t, err)
		assert.Equal(t, 2, len(result))
	})

	t.Run("batchWriteItem request must not be nil", func(t *testing.T) {
		err = model.BatchWriteItem(ctx, nil)
		assert.Error(t, err)
	})

	t.Run("unprocessed target must not be nil", func(t *testing.T) {
		_, err := vogels.NewBatchWriteRequest(nil, []interface{}{posts[0], posts[1]})
		assert.Error(t, err)
	})
	t.Run("unprocessed target must be a pointer", func(t *testing.T) {
		_, err := vogels.NewBatchWriteRequest([]BlogPost{}, []interface{}{posts[0], posts[1]})
		assert.Error(t, err)
	})
	t.Run("unprocessed target must be a slice", func(t *testing.T) {
		_, err := vogels.NewBatchWriteRequest(&BlogPost{}, []interface{}{posts[0], posts[1]})
		assert.Error(t, err)
	})
	t.Run("unprocessed target is set", func(t *testing.T) {
		postOne, err := dynamodbattribute.MarshalMap(posts[0])
		assert.Nil(t, err)
		postTwo, err := dynamodbattribute.MarshalMap(posts[1])
		assert.Nil(t, err)

		tablename := fmt.Sprintf("mockedBlogposts-%s", uid())
		out := dynamodb.BatchWriteItemOutput{
			ConsumedCapacity:      nil,
			ItemCollectionMetrics: nil,
			UnprocessedItems:      map[string][]*dynamodb.WriteRequest{
				tablename: {
					{
						PutRequest: &dynamodb.PutRequest{
							Item: postOne,
						},
					},{
						PutRequest: &dynamodb.PutRequest{
							Item: postTwo,
						},
					},
				},
			},
		}
		model := BlogPost{}.ModelWithClientMock(&dynamoDBClientMock{BatchWriteItemOutput: &out}, tablename)

		unprocessed := []BlogPost{}
		input, err := vogels.NewBatchWriteRequest(&unprocessed, []interface{}{posts[0], posts[1]})
		assert.Nil(t, err)
		err = model.BatchWriteItem(ctx, input)
		assert.Nil(t, err)
		assert.Len(t, unprocessed, 2)
	})
}

func TestScan(t *testing.T) {
	model, _ := createManyTestPosts(75)
	result := []BlogPost{}
	err := model.Scan(context.Background(), &result)
	assert.NoError(t, err)
	assert.Equal(t, 75, len(result))
}

func TestFullScan(t *testing.T) {
	model, _ := createManyTestPosts(1800)
	result := []BlogPost{}
	err := model.Scan(context.Background(), &result)
	assert.NoError(t, err)
	assert.True(t, len(result) < 1500)

	fullResult := []BlogPost{}
	err = model.FullScan(context.Background(), &fullResult)
	assert.NoError(t, err)
	assert.Equal(t, 1800, len(fullResult))
	for _, r := range fullResult {
		assert.NotEmpty(t, r.Name)
		assert.NotEmpty(t, r.Title)
	}
}

func createManyTestPosts(n int) (vogels.ModelAPIV2, []BlogPost) {
	model := BlogPost{}.Model()

	model.CreateTable(ctx)

	posts := make([]BlogPost, n)
	for i := 0; i < n; i++ {
		uuid := uuid.NewV4().String() + "-----------------------------------------------------------------------------------------------------------------"
		posts[i] = BlogPost{Email: "spambot@spammer.com", Title: uuid, Name: uuid + uuid + uuid + uuid}
	}

	for _, post := range posts {
		in := vogels.NewPutItemRequest(post)
		model.PutItem(ctx, in)
	}
	return model, posts
}

func uid() string {
	return uuid.NewV4().String()
}
