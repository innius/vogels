package tests

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"testing"

	"strconv"
	"time"

	"bitbucket.org/innius/vogels"
	"bitbucket.org/innius/go-trn/uuid"
	"github.com/stretchr/testify/assert"
)

type BlogPost struct {
	Email string   `json:"email"`
	Title string   `json:"title"`
	Tags  []string `json"tags"`
	Name  string   `json:"name"`
}

type TimedBlogPost struct {
	Email       string `json:"email"`
	Title       string `json:"title"`
	PublishTime int64  `json:"publish_time"`
}

const PublishTimeIndex = "PublishTime"

func (b BlogPost) Model() vogels.ModelAPIV2 {
	model := vogels.NewModelV2(
		testClient(),
		&vogels.ModelDefinition{
			TableName: "blogposts-" + uuid.NewV4().String(),
			Key:       vogels.NewCompositeKey("email", vogels.StringType, "title", vogels.StringType),
		})
	return model
}

func (b BlogPost) ModelWithClientMock(client dynamodbiface.DynamoDBAPI, tableName string) vogels.ModelAPIV2 {
	model := vogels.NewModelV2(
		client,
		&vogels.ModelDefinition{
			TableName: tableName,
			Key:       vogels.NewCompositeKey("email", vogels.StringType, "title", vogels.StringType),
		})
	return model
}

func (b TimedBlogPost) Model() vogels.ModelAPIV2 {
	model := vogels.NewModelV2(
		testClient(),
		&vogels.ModelDefinition{
			TableName: "NonOverlappingTableName",
			Key:       vogels.NewCompositeKey("email", vogels.StringType, "title", vogels.StringType),
			Indexes: []*vogels.Index{
				{
					Name: PublishTimeIndex,
					Key:  vogels.NewCompositeKey("email", vogels.StringType, "publish_time", vogels.NumberType),
					Type: vogels.LocalIndex,
				},
			},
		})
	return model
}

func TestQuery(t *testing.T) {
	model := BlogPost{}.Model()
	model.CreateTable(ctx)

	posts := createTestPosts(model)

	var items []BlogPost

	model.
		Query(posts[0].Email).
		ExecuteWithContext(ctx, &items)
	assert.Equal(t, 2, len(items))

	res, err := model.
		Query(posts[0].Email).
		Attributes("title", "name").
		Execute(&items)

	assert.Nil(t, err)
	assert.NotNil(t, res)
	assert.Equal(t, 2, len(items))
	assert.Empty(t, items[0].Email)
}

type GameScore struct {
	UserID       string `json:"userid"`
	GameTitle    string `json:"gametitle"`
	TopScore     string `json:"topscore"`
	TopScoreDate int64  `json:"topscoredate"`
	Wins         int    `json:"wins"`
	Losses       int    `json:"losses"`
}

func GameScoreModel() vogels.ModelAPIV2 {
	model := vogels.NewModelV2(
		testClient(),
		&vogels.ModelDefinition{
			TableName: "gamescore",
			Key:       vogels.NewCompositeKey("userid", vogels.StringType, "gametitle", vogels.StringType),
			Indexes: []*vogels.Index{&vogels.Index{
				Name: "GameTitleIndex",
				Key:  vogels.NewCompositeKey("gametitle", vogels.StringType, "topscore", vogels.StringType),
				Type: vogels.GlobalIndex,
			}},
		})
	err := model.CreateTable(ctx)
	if err != nil {
		fmt.Println(err)
	}
	return model
}

func TestQueryWithIndex(t *testing.T) {
	scores := []GameScore{
		{UserID: "Werner Vogels", GameTitle: "Space Invaders", TopScore: "1000", Wins: 1, Losses: 2},
		{UserID: "ronvdw", GameTitle: "Space Invaders", TopScore: "2000", Wins: 1, Losses: 2},
		{UserID: "Werner Vogels", GameTitle: "Pacman", TopScore: "1200", Wins: 2, Losses: 6},
	}
	model := GameScoreModel()
	for _, score := range scores {
		in := vogels.NewPutItemRequest(score)
		_, err := model.PutItem(ctx, in)
		assert.Nil(t, err)
	}
	var items []GameScore
	model.Query("Space Invaders").
		UsingIndex("GameTitleIndex").
		Execute(&items)
}

func TestQueryWithStartKey(t *testing.T) {
	model := BlogPost{}.Model()
	model.CreateTable(ctx)
	email := uuid.NewV4().String() + "@test.com"
	for i := 0; i < 9; i++ {
		post := BlogPost{Email: email, Title: uuid.NewV4().String()}
		_, err := model.PutItem(ctx, vogels.NewPutItemRequest(post))
		assert.NoError(t, err)
	}

	r1 := []BlogPost{}
	qr, err := model.Query(email).Limit(5).ExecuteWithContext(ctx, &r1)
	assert.NoError(t, err)
	assert.Equal(t, 5, len(r1))
	assert.NotEmpty(t, qr.LastKey)
	assert.NoError(t, err)

	r2 := []BlogPost{}
	qr, err = model.Query(email).Limit(5).WithStartKey(qr.LastKey).ExecuteWithContext(ctx, &r2)
	assert.NoError(t, err)
	assert.Equal(t, 4, len(r2))
	assert.Equal(t, 0, len(qr.RawResponse.LastEvaluatedKey))
	assert.Empty(t, qr.LastKey)

	t.Run("with filter expression", func(t *testing.T) {
		r3 := []BlogPost{}
		qr, err = model.Query(email).
			WithStartKey("").
			Filter("name").Equals("").
			Limit(2).
			ExecuteWithContext(ctx, &r3)
		if assert.NoError(t, err) {
			assert.NotEmpty(t, qr.LastKey)
		}
	})
}

func TestQueryWithStartKeyAndIndex(t *testing.T) {
	model := TimedBlogPost{}.Model()
	err := model.CreateTable(context.Background())
	if err != nil {
		println(err.Error())
	}
	email := uuid.NewV4().String() + "@test.com"
	Publish0 := time.Now().Unix()
	for i := 0; i < 9; i++ {
		post := TimedBlogPost{Email: email, Title: uuid.NewV4().String(), PublishTime: Publish0 + int64(i)}
		_, err := model.PutItem(ctx, vogels.NewPutItemRequest(post))
		assert.NoError(t, err)
	}

	r1 := []TimedBlogPost{}
	qr, err := model.Query(email).UsingIndex(PublishTimeIndex).Limit(5).Execute(&r1)
	assert.NoError(t, err)
	assert.Equal(t, 5, len(r1))
	assert.NotEmpty(t, qr.LastKey)

	r2 := []TimedBlogPost{}
	qr, err = model.Query(email).Limit(5).UsingIndex(PublishTimeIndex).WithStartKey(qr.LastKey).Execute(&r2)
	assert.NoError(t, err)
	assert.Equal(t, 4, len(r2))
	assert.Equal(t, 0, len(qr.RawResponse.LastEvaluatedKey))
	assert.Empty(t, qr.LastKey)
}

func createTestPosts(model vogels.ModelAPIV2) []BlogPost {
	//model := BlogPost{}.Model()

	model.CreateTable(ctx)

	posts := []BlogPost{
		{Email: "werner@example.com", Name: "the cloud is great...", Title: "Expanding the Cloud"},
		{Email: "ronvdw@example.com", Name: "you want freedom? ", Title: "Freedom"},
		{Email: "werner@example.com", Name: "dynamodb is awesome", Title: "Streaming support for DynamoDB"},
	}
	for _, post := range posts {
		in := vogels.NewPutItemRequest(post)
		model.PutItem(ctx, in)
	}

	return posts
}

func TestParseLastEvaluatedKey(t *testing.T) {
	model := TimedBlogPost{}.Model()
	err := model.CreateTable(context.Background())
	if err != nil {
		println(err.Error())
	}
	email := uuid.NewV4().String() + "@test.com"
	Publish0 := time.Now().Unix()
	for i := 0; i < 9; i++ {
		post := TimedBlogPost{Email: email, Title: "title" + strconv.FormatInt(int64(i), 10), PublishTime: Publish0 + int64(i)}
		_, err := model.PutItem(ctx, vogels.NewPutItemRequest(post))
		assert.NoError(t, err)
	}

	r1 := []TimedBlogPost{}
	qr, err := model.Query(email).Limit(5).Execute(&r1)

	assert.Equal(t, email+"~title4", qr.LastKey)
}
