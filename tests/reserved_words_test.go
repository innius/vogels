package tests

import (
	"testing"

	"bitbucket.org/innius/vogels"
	"github.com/stretchr/testify/assert"
)

// this struct has reserved words
type identity struct {
	Identity int    `json:"identity"`
	Level    int    `json:"level"`
	Domain   string `json:"domain"`
}

func reservedWordsModel() *vogels.Model {
	return vogels.Define(testClient(), &vogels.ModelDefinition{
		TableName: "reservedwords",
		Key:       vogels.NewCompositeKey("identity", vogels.NumberType, "level", vogels.NumberType),
	})
}

// run the operations on a table with reserved words
func TestReservedWords(t *testing.T) {
	m := reservedWordsModel()
	m.CreateTable(ctx)

	ids := []identity{
		{10, 1, "plantage.io"},
		{10, 2, "plantage.com"},
		{10, 3, "plantage.org"},
	}
	for i := range ids {
		m.PutItem(vogels.NewPutItemRequest(ids[i]))
	}

	t.Run("Get", func(t *testing.T) {
		res := identity{}
		assert.Nil(t, m.Get(ctx, &res, 10, 1, "identity", "level", "domain"))
		assert.Equal(t, 10, res.Identity)
		assert.Equal(t, 1, res.Level)
	})
	t.Run("QueryBetween", func(t *testing.T) {
		res := []identity{}
		_, err := m.Query(10).Where("level").Between(1, 2).Execute(&res)

		assert.Nil(t, err)
		assert.Len(t, res, 2)
	})
	t.Run("Destroy", func(t *testing.T) {
		assert.Nil(t, m.Destroy(ctx, 10, 3))
		res := &identity{}
		assert.Nil(t, m.Get(ctx, res, 10, 3))
		assert.Equal(t, res.Identity, 0)
	})
	t.Run("Update", func(t *testing.T) {
		r := identity{10, 2, "plantage.nu"}
		_, err := m.Update(vogels.NewUpdateRequest(r))
		assert.Nil(t, err)
	})
	t.Run("PutItem", func(t *testing.T) {
		r := identity{10, 2, "plantage.nu"}
		uex, err := vogels.NewUpdateExpressionBuilder(r).Fields("domain").Build()
		assert.Nil(t, err)
		_, err = m.Update(vogels.NewUpdateRequest(r).WithUpdateExpression(uex))
		assert.Nil(t, err)
	})
}
