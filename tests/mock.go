package tests

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

type dynamoDBClientMock struct {
	dynamodbiface.DynamoDBAPI
	BatchWriteItemOutput *dynamodb.BatchWriteItemOutput
}

func (d *dynamoDBClientMock) BatchWriteItemWithContext(aws.Context, *dynamodb.BatchWriteItemInput, ...request.Option) (*dynamodb.BatchWriteItemOutput, error) {
	return d.BatchWriteItemOutput, nil
}
